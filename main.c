#include "main.h" 

// constants

//need a macro to derive the read/write bit for this address.
#define MAKE_ADDR_R(x)   (x | (1<<0))

//PCA9555 IO chip
#define PCA_ADDR_W             0b01000000
#define PCA_ADDR_R             (MAKE_ADDR_R(PCA_ADDR_W))


//configuration ports
#define PCA_CMD_IN_PORT0       0b00000000
#define PCA_CMD_IN_PORT1       0b00000001

#define PCA_CMD_OUT_PORT0      0b00000010
#define PCA_CMD_OUT_PORT1      0b00000011

#define PCA_CMD_POL_PORT0      0b00000100
#define PCA_CMD_POL_PORT1      0b00000101

#define PCA_CMD_CONF_PORT0     0b00000110
#define PCA_CMD_CONF_PORT1     0b00000111

const char* pca_portname_list[] =
  {
    "  PCA_CMD_IN_PORT0", 
    "  PCA_CMD_IN_PORT1", 
    " PCA_CMD_OUT_PORT0", 
    " PCA_CMD_OUT_PORT1", 
    " PCA_CMD_POL_PORT0", 
    " PCA_CMD_POL_PORT1", 
    "PCA_CMD_CONF_PORT0", 
    "PCA_CMD_CONF_PORT1", 
  };
  
const uint8_t pca_port_list[] =
  {
    PCA_CMD_IN_PORT0, 
    PCA_CMD_IN_PORT1, 
    PCA_CMD_OUT_PORT0, 
    PCA_CMD_OUT_PORT1, 
    PCA_CMD_POL_PORT0, 
    PCA_CMD_POL_PORT1, 
    PCA_CMD_CONF_PORT0, 
    PCA_CMD_CONF_PORT1, 
  };



// -------- Global Variables --------- //
volatile uint8_t isIOTriggered = 0;

// -------- Functions --------- //

void i2cWriteData(uint8_t addr_W, uint8_t port, uint8_t data)
{
  i2cStart();
  i2cSend(addr_W);
  i2cSend(port);
  i2cSend(data);
}

//aaah need both address
uint8_t i2cReadDataByte(uint8_t addr_W, uint8_t port)
{
  i2cStart();
  i2cSend(addr_W);
  i2cSend(port);
  i2cStart();

  //right most bit set is read mode
  i2cSend(MAKE_ADDR_R(addr_W));
  
  //Read the byte and tell the sender that we're done receiving data (NoAck) from them.
  uint8_t v = i2cReadNoAck();
  i2cStop();
  return(v);
}

//Pull all register data from chip.
//fill da with results.
void i2cReadChipState(uint8_t addr_W, uint8_t* da)
{
  for(int i = 0; i< 8; i++)
    {
      da[i] = i2cReadDataByte(addr_W, pca_port_list[i]);
    }
  return;
}


void printChipStateDetail(uint8_t* da)
{
  printString("Chip Data:\r\n");
  for(int i = 0; i< 8; i++)
    {
      printString(pca_portname_list[i]);
      printString(":[");
      printHexByte(da[i]);
      printString("]\r\n");
    }
 
}

void printChipState(uint8_t* da)
{
  printString("Chip Data:[");
  for(int i = 0; i< 8; i++)
    {
      printHexByte(da[i]);
      if(i != 7) {printString(" ");}
    }
  printString("]\r\n");
 
}

void reportChipStateDetail(uint8_t addr_W)
{
    uint8_t cs[8] = {0,0,0,0,0,0,0,0};
    i2cReadChipState(addr_W, cs);
    printChipStateDetail(cs);
}

void reportChipState(uint8_t addr_W)
{
    uint8_t cs[8] = {0,0,0,0,0,0,0,0};
    i2cReadChipState(addr_W, cs);
    printChipState(cs);
}

//Overflow timer interruption
ISR(TIMER0_OVF_vect) {
  if(bit_is_clear(BUTTON_PIN, BUTTON) && !isIOTriggered) {
    isIOTriggered = 1;
  }
  else if (bit_is_set(BUTTON_PIN, BUTTON))
    {
      isIOTriggered = 0;
    }
}

//configure a timer to fire periodically... will need to work on the overflow time.
//when overflow, check the button to see if it's been pressed.
void initTimer0(void) {
  // 20mhz clock...need to set a prescaler.
  //  CLKPR |= (1 << CLKPS2); //16
  CLKPR |= (1 << CLKPS2) | (1 << CLKPS1); //64
  //timer
  TCCR0B |= (1 << CS12) | (1 << CS10);
  //overflow
  TIMSK0 |= (1 << TOIE0);
  sei();
}


int main(void) {
  // -------- Inits --------- //
  uint8_t pca_out_data = 0;
  
  // clock_prescale_set(clock_div_1);                 /* CPU Clock: 8 MHz */
  initUSART();
  printString("OK\r\n");
  initI2C();
  printString("I2C Init\r\n");

  reportChipState(PCA_ADDR_W);
 
  //Configure port 0
  printString("I2C:W:P0:config \r\n");
  i2cWriteData(PCA_ADDR_W, PCA_CMD_CONF_PORT0, 0xFF);
  reportChipState(PCA_ADDR_W);

  //Configure polarity port 0
  printString("I2C:W:P0: writing polarity\r\n");
  i2cWriteData(PCA_ADDR_W, PCA_CMD_POL_PORT0, 0xFF);
  reportChipState(PCA_ADDR_W);

  i2cStop();
  
  //configure port 1 for output.
  //Configuration registers (6&7) 1: input; 0: output
  //
  printString("I2C:W:P1:Configure for output\r\n");
  i2cWriteData(PCA_ADDR_W, PCA_CMD_CONF_PORT1, 0x00);
  reportChipState(PCA_ADDR_W);


  //Configure polarity port1
  printString("I2C:W:P1: writing polarity\r\n");
  i2cWriteData(PCA_ADDR_W, PCA_CMD_POL_PORT1, 0x00);
  reportChipState(PCA_ADDR_W);

  //set the leds.
  i2cWriteData(PCA_ADDR_W, PCA_CMD_OUT_PORT1, 0xFF);
  
  i2cStop();

  //now that output is set on port0...
  //Enable the button for input
  BUTTON_PORT |= (1 << BUTTON);

  initTimer0();
  printString("Timer enabled\r\n");

  
  // ------ Event loop ------ //
  while (1) {

    if(isIOTriggered)
      {
	//maybe just read the data from input port and make it available...
	//use main loop to write data...
	reportChipState(PCA_ADDR_W);
	pca_out_data = i2cReadDataByte(PCA_ADDR_W, PCA_CMD_IN_PORT0);
      }

    if(pca_out_data == 2)
      {
	i2cWriteData(PCA_ADDR_W, PCA_CMD_OUT_PORT1, 0x55);
      }
    else
      {
	i2cWriteData(PCA_ADDR_W, PCA_CMD_OUT_PORT1, 0xAA);
      }
  } 

  return (0);                            /* This line is never reached */
}
